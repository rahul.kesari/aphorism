package org.spiritualwave.aphorisms.data.remote

import kotlinx.serialization.Serializable

@Serializable
data class AphorismResponse(
    val id: Int,
    val title: String,
    val summary: String,
    val link: String
)
