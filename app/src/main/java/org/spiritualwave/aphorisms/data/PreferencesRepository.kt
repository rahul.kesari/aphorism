package org.spiritualwave.aphorisms.data

import android.content.Context
import androidx.core.content.edit
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.first
import javax.inject.Inject

private val Context.dataStore by preferencesDataStore(name = Settings.PREFERENCES_NAME)

class PreferencesRepository @Inject constructor(@ApplicationContext context: Context) {

    private val settingsDataStore = context.dataStore

    // Store a string value for a supplied key name
    suspend fun setString(key: String, value: String) {
        val dataStoreKey = stringPreferencesKey(key)
        settingsDataStore.edit { settings ->
            settings[dataStoreKey] = value
        }
    }

    // Read a date value for the key you pass as a parameter
    suspend fun readString(key: String): String? {
        val dataStoreKey = stringPreferencesKey(key)
        val prefs = settingsDataStore.data.first()
        return prefs[dataStoreKey]
    }

    // Store a date value for a supplied key name
    suspend fun setDate(key: String, date: Long) {
        val dataStoreKey = longPreferencesKey(key)
        settingsDataStore.edit { settings ->
            settings[dataStoreKey] = date
        }
    }

    // Read a date value for the key you pass as a parameter
    suspend fun readDate(key: String): Long? {
        val dataStoreKey = longPreferencesKey(key)
        val prefs = settingsDataStore.data.first()
        return prefs[dataStoreKey]
    }

}