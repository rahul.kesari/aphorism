package org.spiritualwave.aphorisms.data

import android.util.Log
import org.spiritualwave.aphorisms.data.entities.UnsplashImage
import org.spiritualwave.aphorisms.data.remote.UnsplashResponse
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.RedirectResponseException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.client.request.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.spiritualwave.aphorisms.ui.util.screenRectPx

class UnsplashRepository(
    private val ktorHttpClient: HttpClient,
    private val backgroundDispatcher: CoroutineDispatcher,
    private val dao: AphorismDAO
) {

    suspend fun getUnsplashImages(count: Int): List<UnsplashImage> = withContext(backgroundDispatcher) {

        val screenWidth = screenRectPx.width()

        try {
            ktorHttpClient
                .prepareGet("https://h5oxda8.spiritualwave.org/api") {
                    header ("AuthToken", "")
                    url {
                        parameters.append("count", count.toString())
                        parameters.append("query", "mountain,waves,sunrise,nature,landscape")
                    }
                }
                .execute { response ->
                    val unsplashImages = response.body<List<UnsplashResponse>>()
                    unsplashImages.map {
                        UnsplashImage(
                            id = it.id,
                            url = it.urls.raw + "&w=$screenWidth",
                            thumb = it.urls.thumb,
                            user = it.user.name,
                            username = it.user.username,
                            blurHash = it.blur_hash,
                            color = it.color
                        )
                    }
                }
        } catch (e: ServerResponseException) {
            Log.d("Ktor", "Error getting Unsplash images: ${e.response.status.description}")
            emptyList()
        } catch (e: ClientRequestException) {
            Log.d("Ktor", "Error getting Unsplash images: ${e.response.status.description}")
            emptyList()
        } catch (e: RedirectResponseException) {
            Log.d("Ktor", "Error getting Unsplash images: ${e.response.status.description}")
            emptyList()
        }

    }

    suspend fun insertUnsplashImages(images: List<UnsplashImage>) {
        dao.insertAllUnsplashImages(images)
    }

}