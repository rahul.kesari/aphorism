package org.spiritualwave.aphorisms.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate

@Entity(tableName = "aphorism")
data class Aphorism(
    @PrimaryKey(autoGenerate = false) val id: Int,
    val title: String,
    val summary: String,
    val link: String,
    val date: LocalDate? = null,
    val isFavourite: Boolean = false,
    val imageId: String? = null
) {

    val isValid: Boolean
        get() = id > 0

    constructor() : this(
        -1,
        "",
        "",
        ""
    )

    companion object {
        val PARAM_ID = "param_id"
        val PARAM_TITLE = "param_title"
        val PARAM_DATE = "param_date"
        val PARAM_SUMMARY = "param_summary"
        val PARAM_LINK = "param_link"

        fun fromDataStore(
            id: Int?,
            title: String?,
            dateInMillis: Long?,
            summary: String?,
            link: String?
        ): Aphorism =
            Aphorism(
                id = id ?: -1,
                title = title ?: "Няма зло, има нееволюирало добро",
                date = LocalDate.ofEpochDay(dateInMillis ?: -1482710400), // 1923-01-07
                summary = summary ?: "",
                link = link ?: ""
            )
    }
}
