package org.spiritualwave.aphorisms.data

import androidx.room.*
import org.spiritualwave.aphorisms.data.entities.Aphorism
import org.spiritualwave.aphorisms.data.entities.UnsplashImage
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

@Dao
interface AphorismDAO {

    // APHORISMS

    @Transaction
    @Query("SELECT * FROM aphorism WHERE id = :aphorismId")
    suspend fun getAphorismById(aphorismId: Int): AphorismAndImage

    @Transaction
    @Query("SELECT * FROM aphorism WHERE date = :date")
    fun getAphorismByDate(date: LocalDate): Flow<AphorismAndImage>

    @Transaction
    @Query("SELECT * FROM aphorism WHERE date < :date ORDER BY date DESC")
    fun getPreviousAphorisms(date: LocalDate): Flow<List<AphorismAndImage>>

    @Transaction
    @Query("SELECT * FROM aphorism WHERE isFavourite = 1 ORDER BY title ASC")
    fun getFavouriteAphorisms(): Flow<List<AphorismAndImage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAphorism(aphorism: Aphorism)

    @Transaction
    @Query("SELECT * FROM aphorism ORDER BY date DESC")
    fun getAllAphorisms(): Flow<List<AphorismAndImage>>

    @Transaction
    @Query("SELECT COUNT(*) FROM aphorism")
    suspend fun countAllAphorisms(): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAllAphorisms(aphorisms: List<Aphorism>)

    @Query("UPDATE aphorism SET date = :date WHERE id = :id")
    suspend fun updateDate(id: Int, date: LocalDate)

    @Transaction
    @Query("SELECT isFavourite FROM aphorism WHERE id = :aphorismId")
    suspend fun getFavouriteStatus(aphorismId: Int): Boolean

    @Query("UPDATE aphorism SET isFavourite = :isFavourite WHERE id = :id")
    suspend fun toggleFavourite(isFavourite: Boolean, id: Int)

    @Transaction
    @Query("SELECT * FROM aphorism WHERE NULLIF(date, '') IS NULL")
    suspend fun getAphorismsWithoutDate(): List<Aphorism>

    @Transaction
    @Query("SELECT MAX(date) FROM aphorism")
    suspend fun getMaxDate(): LocalDate?

    @Transaction
    @Query("SELECT * FROM aphorism WHERE NULLIF(imageId, '') IS NULL ORDER BY date ASC")
    suspend fun getAphorismsWithoutImage(): List<Aphorism>

    @Query("UPDATE aphorism SET imageId = :imageId WHERE id = :aphorismId")
    suspend fun addImageId(aphorismId: Int, imageId: String)

    // UNSPLASH
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUnsplashImage(image: UnsplashImage)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAllUnsplashImages(images: List<UnsplashImage>)




}