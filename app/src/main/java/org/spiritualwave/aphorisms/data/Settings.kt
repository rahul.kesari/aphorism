package org.spiritualwave.aphorisms.data

object Settings {

    const val PREFERENCES_NAME = "aphorisms_preferences"
    const val LAST_UPDATE_CHECK_DATE = "last_update_check_date"
    const val LOCAL_HASH_STRING = "local_hash_string"
    const val NOTIFICATION_TRIGGER_TIME = "notification_trigger_time"
}