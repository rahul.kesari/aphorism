package org.spiritualwave.aphorisms.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.spiritualwave.aphorisms.data.entities.Aphorism
import org.spiritualwave.aphorisms.data.entities.UnsplashImage
import org.spiritualwave.aphorisms.ui.util.DateConverter

@Database(
    entities = [
        Aphorism::class,
        UnsplashImage::class
    ],
    version = 1
)

@TypeConverters(DateConverter::class)

abstract class AphorismDatabase: RoomDatabase() {

    abstract fun aphorismDAO(): AphorismDAO

}