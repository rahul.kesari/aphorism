package org.spiritualwave.aphorisms.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "unsplash")
data class UnsplashImage(
    @PrimaryKey(autoGenerate = false) val id: String,
    val url: String,
    val thumb: String,
    val user: String,
    val username: String,
    val blurHash: String,
    val color: String
)
