package org.spiritualwave.aphorisms.data.remote

import kotlinx.serialization.Serializable

@Serializable
data class UnsplashResponse(
    val id: String,
    val color: String,
    val blur_hash: String,
    val urls: UnsplashUrls,
    val user: UnsplashUser
)


@Serializable
data class UnsplashUrls(
    val raw: String,
    val thumb: String
)

@Serializable
data class UnsplashUser (
    val username: String,
    val name: String
)