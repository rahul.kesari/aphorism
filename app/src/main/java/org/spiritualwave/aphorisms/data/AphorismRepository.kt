package org.spiritualwave.aphorisms.data

import android.util.Log
import org.spiritualwave.aphorisms.data.entities.Aphorism
import org.spiritualwave.aphorisms.data.entities.UnsplashImage
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import org.spiritualwave.aphorisms.data.remote.AphorismResponse
import io.ktor.client.HttpClient
import io.ktor.client.call.*
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.RedirectResponseException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.isSuccess
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import java.time.LocalDate

class AphorismRepository(
    private val ktorHttpClient: HttpClient,
    private val backgroundDispatcher: CoroutineDispatcher,
    private val dao: AphorismDAO
) {

    suspend fun getAphorism(aphorismId: Int): AphorismAndImage = dao.getAphorismById(aphorismId = aphorismId)

    fun getDailyAphorism(date: LocalDate): Flow<AphorismAndImage> = dao.getAphorismByDate(date)

    fun getAllLocal(): Flow<List<AphorismAndImage>> = dao.getAllAphorisms()

    fun getPreviousAphorisms(): Flow<List<AphorismAndImage>> {
        val today = LocalDate.now()
        return dao.getPreviousAphorisms(today)
    }

    fun getFavouritephorisms(): Flow<List<AphorismAndImage>> {
        return dao.getFavouriteAphorisms()
    }

    suspend fun countAllAphorisms(): Int {
        return dao.countAllAphorisms()
    }

    suspend fun getAphorismsWithoutImage(): List<Aphorism> {
        return dao.getAphorismsWithoutImage()
    }

    suspend fun getAphorismsWithoutDate(): List<Aphorism> {
        return dao.getAphorismsWithoutDate()
    }

    suspend fun updateAphorismDate(aphorisms: List<Aphorism>) {

//        val startDate = LocalDate.now()
        val startDate = dao.getMaxDate()?.plusDays(1) ?: LocalDate.now()
//        Log.d("VAKLUSH", "Updating aphorisms' dates. Start date: $startDate")
        var newDate: LocalDate

        for ((increment, aphorism) in aphorisms.withIndex()) {
            newDate = LocalDate.from(startDate).plusDays(increment.toLong())

            dao.updateDate(aphorism.id, newDate)
//            Log.d("VAKLUSH", "Updated date of aphorism: ${aphorism.id} to $newDate")

        }
    }

    suspend fun addSingleImageIds(aphorism: Aphorism, images: List<UnsplashImage>) {
        dao.addImageId(aphorism.id, images.first().id)
    }

    suspend fun addImageIds(aphorisms: List<Aphorism>, images: List<UnsplashImage>) {

        if (aphorisms.size <= images.size) {
            for (i in aphorisms.indices) {
                dao.addImageId(aphorisms[i].id, images[i].id)
//                Log.d("VAKLUSH", "Added image to: ${aphorisms[i].id} to ${images[i].id}")
            }
        }
    }

    // Get the SHA256 hash string for the remote aphorisms.json file
    suspend fun getHash(): String {

        var remoteHash = ""

        ktorHttpClient
            .prepareGet("https://spiritualwave.org/app/aphorisms.hash")
            .execute { response: HttpResponse ->
                if (response.status.isSuccess()) {
                    remoteHash = response.bodyAsText()
                }
            }

        return remoteHash
    }

    // Get the content of the remote aphorisms.json file
    suspend fun getAllRemote(): List<Aphorism> = withContext(backgroundDispatcher) {

        try {
            ktorHttpClient
                .prepareGet("https://spiritualwave.org/app/aphorisms.json")
                .execute { response: HttpResponse ->
                    val remoteAphorisms = response.body<List<AphorismResponse>>()
//                Log.d("VAKLUSH", "Number of remote aphorisms: ${remoteAphorisms.size}")
                    remoteAphorisms.map { remote ->
                        Aphorism(
                            id = remote.id,
                            title = remote.title,
                            summary = remote.summary,
                            link = remote.link
                        )
                    }
                }
        } catch (e: ServerResponseException) {
            Log.d("Ktor", "Error getting remote aphorisms: ${e.response.status.description}")
            emptyList<Aphorism>()
        } catch (e: ClientRequestException) {
            Log.d("Ktor", "Error getting remote aphorisms: ${e.response.status.description}")
            emptyList<Aphorism>()
        } catch (e: RedirectResponseException) {
            Log.d("Ktor", "Error getting remote aphorisms: ${e.response.status.description}")
            emptyList<Aphorism>()
        }

    }


    suspend fun insertAllRemote(aphorisms: List<Aphorism>) {
        dao.insertAllAphorisms(aphorisms)
    }

    suspend fun toggleFavourite(aphorism: Aphorism) {
        val isFavourite = !aphorism.isFavourite
        dao.toggleFavourite(isFavourite, aphorism.id)
    }

    suspend fun getFavouriteStatus(aphorismId: Int): Boolean {
        return dao.getFavouriteStatus(aphorismId)
    }

}