package org.spiritualwave.aphorisms.data.relations

import androidx.room.Embedded
import androidx.room.Relation
import org.spiritualwave.aphorisms.data.entities.Aphorism
import org.spiritualwave.aphorisms.data.entities.UnsplashImage

data class AphorismAndImage(
    @Embedded val aphorism: Aphorism,
    @Relation(
        parentColumn = "imageId",
        entityColumn = "id"
    )
    val image: UnsplashImage? = null
) {
    constructor(): this (
        Aphorism(
            id = 1,
            title = "Няма зло, има нееволюирало добро",
            summary = "Текст",
            link = ""
        ),
        UnsplashImage(
            id = "",
            url = "",
            thumb = "",
            username = "",
            user = "",
            blurHash = "",
            color = ""
        )
    )

    companion object {
        val SampleAphorism = AphorismAndImage(
            Aphorism(
                id = 1,
                title = "Няма зло, има нееволюирало добро",
                summary = "Текст",
                link = ""
            ),
            UnsplashImage(
                id = "",
                url = "",
                thumb = "",
                username = "",
                user = "",
                blurHash = "",
                color = ""
            )
        )
    }

    fun matchesSearchQuery(query: String): Boolean {

        val matchingCombinations = listOf(
            aphorism.title,
            aphorism.summary
        )
        return matchingCombinations.any {
            it.contains(query, ignoreCase = true)
        }
    }
}
