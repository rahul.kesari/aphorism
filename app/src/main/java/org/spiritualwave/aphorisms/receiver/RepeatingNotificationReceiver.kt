package org.spiritualwave.aphorisms.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.data.PreferencesRepository
import org.spiritualwave.aphorisms.data.Settings
import org.spiritualwave.aphorisms.utils.AlarmUtils
import org.spiritualwave.aphorisms.utils.AphorismsCoroutineScope
import org.spiritualwave.aphorisms.utils.NotificationUtils
import java.time.LocalDate
import javax.inject.Inject

@AndroidEntryPoint
class RepeatingNotificationReceiver : BroadcastReceiver() {

    @Inject
    lateinit var preferenceRepository: PreferencesRepository

    @Inject
    lateinit var repository: AphorismRepository
    private val coroutineScope = AphorismsCoroutineScope()

    private val TAG = RepeatingNotificationReceiver::class.simpleName
    override fun onReceive(context: Context, intent: Intent) {

        Log.d(TAG, "onReceive ${intent.action}")
        when (intent.action) {
            Intent.ACTION_BOOT_COMPLETED -> {
                Log.d(TAG, "onReceive Boot Completed")
                // schedule alarms again after device restart because the alam gets canceled by Android OS once device restarts.
                coroutineScope.launch {
                    preferenceRepository.readString(Settings.NOTIFICATION_TRIGGER_TIME)
                        ?.also { times ->
                            AlarmUtils.setRepeatingNotifications(
                                context,
                                times
                            )
                        }
                }
            }

            AlarmUtils.ACTION_ALARM_TRIGGERED -> {
                // 1- Alarm Triggered for the user selected time
                // 2- read quote of the day from database
                // 3- generate local notification.
                Log.d(TAG, "onReceive ACTION_ALARM_TRIGGERED")
                coroutineScope.launch {
                    repository.getDailyAphorism(LocalDate.now()).collect {
                        val aphorism = it.aphorism
                        Log.d(TAG, "onReceive $aphorism")
                        NotificationUtils.showNotification(
                            context,
                            aphorism.title,
                            aphorism.summary
                        )

                        // stop collecting data once we read and generate notification
                        // coroutineScope.cancel()
                    }
                    // set for next day
                    preferenceRepository.readString(Settings.NOTIFICATION_TRIGGER_TIME)
                        ?.also { times ->
                            AlarmUtils.setRepeatingNotifications(
                                context,
                                times,
                                true
                            )
                        }

                }
            }
        }
    }
}
