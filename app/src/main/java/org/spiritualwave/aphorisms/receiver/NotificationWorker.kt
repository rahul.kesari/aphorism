package org.spiritualwave.aphorisms.receiver

import android.content.Context
import android.util.Log
import androidx.hilt.work.HiltWorker
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.data.PreferencesRepository
import org.spiritualwave.aphorisms.utils.AphorismsCoroutineScope
import org.spiritualwave.aphorisms.utils.NotificationUtils
import java.time.LocalDate
import java.util.Calendar
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltWorker
class NotificationWorker @AssistedInject constructor(
    @Assisted private val context: Context,
    @Assisted params: WorkerParameters
) : Worker(context, params) {

    @Inject
    lateinit var preferenceRepository: PreferencesRepository

    @Inject
    lateinit var repository: AphorismRepository
    private val coroutineScope = AphorismsCoroutineScope()

    private val TAG = NotificationWorker::class.simpleName

    override fun doWork(): Result {
        Log.d(TAG, "doWork called")
        // Create and show your notification here
        coroutineScope.launch {
            repository.getDailyAphorism(LocalDate.now()).collect {
                val aphorism = it.aphorism
                Log.d(TAG, "doWork $aphorism")
                NotificationUtils.showNotification(
                    context,
                    aphorism.title,
                    aphorism.summary
                )

                // stop collecting data once we read and generate notification
                // coroutineScope.cancel()
            }
        }
        // Return the result of the work
        return Result.success()
    }

    companion object {
        private const val WORK_TAG = "notification_work"
        private val TAG = NotificationWorker::class.simpleName

//        fun scheduleNotificationWork(context: Context, times: String) {
//            Log.d(TAG, "scheduleNotificationWork $times")
//            val triggerTimeMillis = calculateTriggerTimeInMillis(times)
//            Log.d(TAG, "triggerTimeMillis $triggerTimeMillis")
//            val constraints = Constraints.Builder()
//                .build()
//
//            val notificationWorkRequest =
//                PeriodicWorkRequestBuilder<NotificationWorker>(1, TimeUnit.DAYS)
//                    .setInitialDelay(
//                        triggerTimeMillis,
//                        TimeUnit.MILLISECONDS
//                    )
//                    .setConstraints(constraints)
//                    .addTag(WORK_TAG)
//                    .build()
//
//            WorkManager.getInstance(context).enqueueUniquePeriodicWork(
//                WORK_TAG,
//                ExistingPeriodicWorkPolicy.KEEP,
//                notificationWorkRequest
//            )
//        }

        fun cancelAll(context: Context) {
            Log.d(TAG, "cancelAll")
            WorkManager.getInstance(context).cancelAllWorkByTag(WORK_TAG)
        }
    }
}
