package org.spiritualwave.aphorisms.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavouriteAphorismsViewModel @Inject constructor (
    private val repository: AphorismRepository
): ViewModel() {

    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    private val _favouriteAphorisms = MutableStateFlow(listOf<AphorismAndImage>())
    val favouriteAphorisms = searchText
        .combine(_favouriteAphorisms) { text, aphorisms ->
            if (text.isBlank()) {
                aphorisms
            } else {
                aphorisms.filter {
                    it.matchesSearchQuery(text)
                }
            }
        }
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(5000),
            _favouriteAphorisms.value
        )


    init {
        loadFavouriteAphorisms()
    }

    private fun loadFavouriteAphorisms() {
        viewModelScope.launch {
            repository.getFavouritephorisms().collect {
                _favouriteAphorisms.value = it
            }
        }
    }

    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }

    fun clearSearchText() {
        _searchText.value = ""
    }
}