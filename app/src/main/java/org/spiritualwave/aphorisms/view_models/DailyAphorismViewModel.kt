package org.spiritualwave.aphorisms.view_models

import android.util.Log
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.data.UnsplashRepository
import org.spiritualwave.aphorisms.data.entities.Aphorism
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.spiritualwave.aphorisms.data.PreferencesRepository
import org.spiritualwave.aphorisms.data.Settings
import java.time.LocalDate
import java.time.Period
import javax.inject.Inject

@HiltViewModel
class DailyAphorismViewModel @Inject constructor (
    private val repository: AphorismRepository,
    private val unsplashRepository: UnsplashRepository,
    private val preferencesRepository: PreferencesRepository
): ViewModel() {

    private val _tag = "VAKLUSH"

    private var aphorismsWithoutDate = mutableStateOf<List<Aphorism>>(listOf())
    var countNewAphorisms = mutableStateOf(0)

    private var aphorismsWithoutImage = mutableStateOf<List<Aphorism>>(listOf())

    private val today = LocalDate.now()
    private var _dailyAphorism = MutableStateFlow<AphorismAndImage?>(null)
    val dailyAphorism = _dailyAphorism.asStateFlow()

    private val _isSnackBarShown = MutableSharedFlow<Boolean>()
    val isSnackBarShownFlow = _isSnackBarShown.asSharedFlow()

    init {
        load()

        loadRemoteAphorisms()
    }

    private fun load() {

        // Set dates and get aphorism of the day
        viewModelScope.launch {

            aphorismsWithoutDate.value = repository.getAphorismsWithoutDate()
            Log.d(_tag, "Aphorisms without date: ${aphorismsWithoutDate.value.size}")

            if (aphorismsWithoutDate.value.isNotEmpty()) {
                repository.updateAphorismDate(aphorismsWithoutDate.value.shuffled())
            }

            repository.getDailyAphorism(today).collect {
                _dailyAphorism.value = it
                if (it.aphorism.imageId.isNullOrBlank()) {
                    val image = try {
                        unsplashRepository.getUnsplashImages(1)
                    } catch (e: Exception) {
                        Log.d("Ktor", "Error getting daily aphorism Unsplash images: ${e.message}")
                        emptyList()
                    }

                    if (image.isNotEmpty()) {
                        unsplashRepository.insertUnsplashImages(image)
                        repository.addSingleImageIds(it.aphorism, image)
                        Log.d(_tag, "Daily aphorism image updated")
                    }
                }
            }

        }

        // Add images
        viewModelScope.launch {

            aphorismsWithoutImage.value = repository.getAphorismsWithoutImage()
            Log.d(_tag, "Aphorisms w/o images: ${aphorismsWithoutImage.value.size}")

            if (aphorismsWithoutImage.value.isNotEmpty()) {
                val imageCount = if (aphorismsWithoutImage.value.size > 30) 30 else aphorismsWithoutImage.value.size
                val images = try {
                    unsplashRepository.getUnsplashImages(imageCount)
                } catch (e: Exception) {
                    Log.d("Ktor", "Error getting Unsplash images: ${e.message}")
                    emptyList()
                }

                if (images.isNotEmpty()) {
                    unsplashRepository.insertUnsplashImages(images)
                    repository.addImageIds(aphorismsWithoutImage.value, images)
                    Log.d(_tag, "Images inserted: $imageCount")
                }
            }
        }
    }

    private fun loadRemoteAphorisms() {
        Log.d(_tag, "===INITIATING REMOTE LOAD===")
        viewModelScope.launch {
            val lastUpdateCheck = preferencesRepository.readDate(Settings.LAST_UPDATE_CHECK_DATE)
            val lastUpdateCheckDate = LocalDate.ofEpochDay(lastUpdateCheck ?: today.toEpochDay() )
            Log.d(_tag, "Last update check: $lastUpdateCheck")
            Log.d(_tag, "Last update check date: $lastUpdateCheckDate")

            val period = Period.between(lastUpdateCheckDate, today)
            Log.d(_tag, "Days since last check: ${period.days}")

            // No last update date, perform check
            if (lastUpdateCheck == null || period.days >= 1) {

                val remoteHash = try {
                    repository.getHash()
                } catch (e: Exception) {
                    Log.d("Ktor", "Error getting remote hash: ${e.message}")
                    ""
                }
                Log.d(_tag, "Remote hash: $remoteHash")

                val localHash = preferencesRepository.readString(Settings.LOCAL_HASH_STRING)
                Log.d(_tag, "Local hash: $localHash")

                if (remoteHash != "" && remoteHash != localHash) {

                    val remoteAphorisms = try {
                        repository.getAllRemote()
                    } catch (e: Exception) {
                        Log.d("Ktor", "Error getting remote aphorisms: ${e.message}")
                        emptyList()
                    }

                    if (remoteAphorisms.isNotEmpty()) {
                        repository.insertAllRemote(remoteAphorisms)
                        preferencesRepository.setString(Settings.LOCAL_HASH_STRING, remoteHash)
                        preferencesRepository.setDate(Settings.LAST_UPDATE_CHECK_DATE, today.toEpochDay())

                        countNewAphorisms.value = repository.getAphorismsWithoutDate().size
                        _isSnackBarShown.emit(true)
                    }
                }
            }
        }
    }
}