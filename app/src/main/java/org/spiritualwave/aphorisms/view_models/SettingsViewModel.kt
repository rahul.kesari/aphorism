package org.spiritualwave.aphorisms.view_models

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.PowerManager
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.spiritualwave.aphorisms.data.PreferencesRepository
import org.spiritualwave.aphorisms.data.Settings
import org.spiritualwave.aphorisms.utils.AlarmUtils
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val repository: PreferencesRepository
) : ViewModel() {

    private val _notificationTime = MutableStateFlow<String>("")
    val notificationTime = _notificationTime.asStateFlow()

    private val _showBatteryOptimizationAlert = MutableStateFlow<Boolean>(false)
    val showBatteryOptimizationAlert = _showBatteryOptimizationAlert.asStateFlow()

    private val TAG = SettingsViewModel::class.simpleName

    init {
        viewModelScope.launch {
            _notificationTime.value =
                repository.readString(Settings.NOTIFICATION_TRIGGER_TIME) ?: ""
        }
    }

    fun enableAndSaveNotificationTime(time: String) {
        showBatteryOptimizationMessage()
        viewModelScope.launch {
            _notificationTime.value = time
            repository.setString(Settings.NOTIFICATION_TRIGGER_TIME, time)
            AlarmUtils.cancelAlarm(context)
            AlarmUtils.setRepeatingNotifications(context, time)
        }
    }

    private fun showBatteryOptimizationMessage() {
        Log.d(TAG, "showBatteryOptimizationMessage ${isBatteryOptimizationEnabled(context)}")
        if (isBatteryOptimizationEnabled(context)) {
            _showBatteryOptimizationAlert.value = true
        }
    }

    fun showBatteryOptimizationSettings(){
        val packageName = context.packageName
        val intent = Intent()
        intent.action = android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = Uri.parse("package:$packageName")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    private fun isBatteryOptimizationEnabled(context: Context): Boolean {
        val powerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val packageName = context.packageName
        return !powerManager.isIgnoringBatteryOptimizations(packageName)
    }

    fun disableNotification() {
        viewModelScope.launch {
            _notificationTime.value = ""
            repository.setString(Settings.NOTIFICATION_TRIGGER_TIME, "")
            AlarmUtils.cancelAlarm(context)
        }
    }

    fun handleAlertVisibility(showDialog: Boolean) {
        _showBatteryOptimizationAlert.value = showDialog
    }
}