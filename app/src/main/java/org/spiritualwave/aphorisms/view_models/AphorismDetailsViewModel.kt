package org.spiritualwave.aphorisms.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.data.entities.Aphorism
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AphorismDetailsViewModel @Inject constructor (
    private val repository: AphorismRepository
): ViewModel() {

    private val _isFavourite = MutableStateFlow(false)
    val isFavourite = _isFavourite.asStateFlow()

    suspend fun getAphorismDetails(aphorismId: Int): AphorismAndImage {
        val aphorism = repository.getAphorism(aphorismId)
        _isFavourite.value = aphorism.aphorism.isFavourite
        return aphorism
    }

    fun toggleFavourite(aphorism: Aphorism) {
        viewModelScope.launch {
            repository.toggleFavourite(aphorism = aphorism)

            val favouriteStatus = repository.getFavouriteStatus(aphorism.id)
            _isFavourite.value = favouriteStatus
        }
    }
}