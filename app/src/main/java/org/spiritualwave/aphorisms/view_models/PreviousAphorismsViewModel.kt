package org.spiritualwave.aphorisms.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PreviousAphorismsViewModel @Inject constructor (
    private val repository: AphorismRepository
): ViewModel() {

    // SEARCH
    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    private var _previousAphorisms = MutableStateFlow(listOf<AphorismAndImage>())
    val previousAphorisms = searchText
        .combine(_previousAphorisms) { text, aphorisms ->
            if (text.isBlank()) {
                aphorisms
            } else {
                aphorisms.filter {
                    it.matchesSearchQuery(text)
                }
            }
        }
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(5000),
            _previousAphorisms.value
        )


    init {
        loadAphorisms()
    }

    private fun loadAphorisms() {

        viewModelScope.launch {

            repository.getPreviousAphorisms().collect {
                _previousAphorisms.value = it
            }
        }
    }

    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }

    fun clearSearchText() {
        _searchText.value = ""
    }
}