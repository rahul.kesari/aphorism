package org.spiritualwave.aphorisms

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.calculateEndPadding
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SheetValue
import androidx.compose.material3.rememberBottomSheetScaffoldState
import androidx.compose.material3.rememberStandardBottomSheetState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import androidx.work.WorkManager
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import dagger.hilt.android.AndroidEntryPoint
import org.spiritualwave.aphorisms.ui.navigation.ScreenContainer
import org.spiritualwave.aphorisms.ui.navigation.tabbar.TabBar
import org.spiritualwave.aphorisms.ui.theme.AphorismsTheme
import org.spiritualwave.aphorisms.utils.NotificationUtils
import org.spiritualwave.aphorisms.view_models.SearchAppBarViewModel
import org.spiritualwave.aphorisms.view_models.SplashScreenViewModel
import org.spiritualwave.aphorisms.widget.update.WidgetUpdater

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val splashScreenViewModel: SplashScreenViewModel by viewModels()
    private val searchAppBarViewModel: SearchAppBarViewModel by viewModels()

    private lateinit var widgetUpdater: WidgetUpdater
    private lateinit var permissionLauncher : ActivityResultLauncher<Array<String>>
    private var isScheduleAlarmGranted = false
    private var isPostNotificationGranted = false

    @OptIn(ExperimentalAnimationApi::class, ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {

//        val fontScale = resources.configuration.fontScale
        val splashScreen = installSplashScreen()

        WindowCompat.setDecorFitsSystemWindows(window, false)

        super.onCreate(savedInstanceState)

        val workManager: WorkManager = WorkManager.getInstance(this)

        widgetUpdater = WidgetUpdater(workManager)
        widgetUpdater.enqueuePeriodicWorker()
        // Load Splash screen based on View Model's isLoading property
        splashScreen.setKeepOnScreenCondition {
            splashScreenViewModel.isLoading.value
        }

        setContent {

            AphorismsTheme {
                val sheetState = rememberStandardBottomSheetState(
                    initialValue = SheetValue.PartiallyExpanded
                )
                val scaffoldState = rememberBottomSheetScaffoldState(
                    bottomSheetState = sheetState
                )
                val navController = rememberAnimatedNavController()


                Scaffold(
                    bottomBar = {
                        TabBar(
                            navController = navController,
                            onItemClick = { navController.navigate(it.route) }
                        )
                    },
                    modifier = Modifier
                        .fillMaxSize()
                ) { innerPadding ->
                    val ld: LayoutDirection = LayoutDirection.Ltr
                    ScreenContainer(
                        navController = navController,
                        innerPaddingValues = PaddingValues(
                            start = innerPadding.calculateStartPadding(ld),
                            top = 0.dp,
                            end = innerPadding.calculateEndPadding(ld),
                            bottom = innerPadding.calculateBottomPadding()
                        ),
                        searchAppBarViewModel = searchAppBarViewModel,
                        scaffoldState = scaffoldState
                    )
                }
            }
        }

//        Log.d("VAKLUSH", "Font scale: $fontScale")
        widgetUpdater.enqueueOneTimeWorker()

        requestPermission()
    }

    override fun onPause() {
        super.onPause()
        widgetUpdater.enqueueOneTimeWorker()
    }

    private fun requestPermission() {
        if (android.os.Build.VERSION.SDK_INT < 33)
            return
        permissionLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){ permisions ->
            isPostNotificationGranted =  permisions[Manifest.permission.USE_EXACT_ALARM] ?: isPostNotificationGranted
            isScheduleAlarmGranted =  permisions[Manifest.permission.POST_NOTIFICATIONS] ?: isScheduleAlarmGranted
        }
        isPostNotificationGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.USE_EXACT_ALARM) == PackageManager.PERMISSION_GRANTED
        isScheduleAlarmGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED

       val permissions = buildList {
            add(Manifest.permission.USE_EXACT_ALARM).takeIf { !isScheduleAlarmGranted }
            add(Manifest.permission.POST_NOTIFICATIONS).takeIf { !isPostNotificationGranted }
        }
        permissionLauncher.launch(permissions.toTypedArray())
    }
}