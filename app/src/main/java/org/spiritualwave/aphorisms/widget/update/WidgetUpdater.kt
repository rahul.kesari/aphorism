package org.spiritualwave.aphorisms.widget.update

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequest
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.concurrent.TimeUnit

class WidgetUpdater(
    private val workManager: WorkManager
) {

    fun enqueueOneTimeWorker() {
        workManager.enqueueUniqueWork(
            UNIQUE_WIDGET_UPDATE_TASK,
            ExistingWorkPolicy.REPLACE,
            buildOneTimeRequest()
        )
    }

    fun enqueuePeriodicWorker() {
        workManager.enqueueUniquePeriodicWork(
            DAILY_WIDGET_UPDATE_TASK,
            ExistingPeriodicWorkPolicy.KEEP,
            buildPeriodicRequest()
        )
    }

    private fun buildPeriodicRequest(): PeriodicWorkRequest {
        return PeriodicWorkRequestBuilder<DailyWidgetUpdaterTask>(30, TimeUnit.MINUTES)
//            .setInitialDelay(hoursUntilMidnight())
            .addTag(DAILY_WIDGET_UPDATE_TASK)
            .build()
    }

    private fun buildOneTimeRequest(): OneTimeWorkRequest =
        OneTimeWorkRequestBuilder<DailyWidgetUpdaterTask>()
            .build()


//    private fun hoursUntilMidnight(): Duration {
//        val now = LocalDateTime.now()
//        val midnight = LocalDateTime.of(now.toLocalDate(), LocalTime.MIDNIGHT) + Duration.ofMinutes(30)
//        return Duration.between(now, midnight).abs()
//    }

    companion object {
        const val DAILY_WIDGET_UPDATE_TASK = "daily_widget_update_task"
        const val UNIQUE_WIDGET_UPDATE_TASK = "unique_widget_update_task"
    }

}