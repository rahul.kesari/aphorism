package org.spiritualwave.aphorisms.widget.update

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.widget.WidgetUtils
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.firstOrNull
import java.time.LocalDate

@HiltWorker
internal class DailyWidgetUpdaterTask @AssistedInject constructor(
    @Assisted private val context: Context,
    @Assisted workParams: WorkerParameters,
    private val aphorismRepository: AphorismRepository
) : CoroutineWorker(context, workParams) {

    override suspend fun doWork(): Result {
        val newAphorism =
            aphorismRepository.getDailyAphorism(LocalDate.now()).firstOrNull()?.aphorism
        return if (newAphorism != null) {
            try {
                WidgetUtils.updateGlanceWidget(context, newAphorism)
                Result.success()
            } catch (e: Exception) {
                Result.retry()
            }
        } else Result.retry()
    }
}