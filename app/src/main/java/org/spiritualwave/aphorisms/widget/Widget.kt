package org.spiritualwave.aphorisms.widget


import android.os.Build
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.glance.GlanceModifier
import androidx.glance.Image
import androidx.glance.ImageProvider
import androidx.glance.LocalSize
import androidx.glance.action.actionStartActivity
import androidx.glance.action.clickable
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.SizeMode
import androidx.glance.appwidget.cornerRadius
import androidx.glance.background
import androidx.glance.currentState
import androidx.glance.layout.Alignment
import androidx.glance.layout.Box
import androidx.glance.layout.Column
import androidx.glance.layout.Row
import androidx.glance.layout.Spacer
import androidx.glance.layout.fillMaxHeight
import androidx.glance.layout.fillMaxSize
import androidx.glance.layout.fillMaxWidth
import androidx.glance.layout.padding
import androidx.glance.layout.width
import androidx.glance.text.FontStyle
import androidx.glance.text.FontWeight
import androidx.glance.text.Text
import androidx.glance.text.TextAlign
import androidx.glance.text.TextStyle
import androidx.glance.unit.ColorProvider
import androidx.glance.unit.FixedColorProvider
import org.spiritualwave.aphorisms.MainActivity
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.data.entities.Aphorism
import org.spiritualwave.aphorisms.ui.theme.md_theme_dark_onSurfaceVariant
import org.spiritualwave.aphorisms.widget.update.CustomGlanceStateDefinition
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class Widget : GlanceAppWidget() {

    private val widgetDateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("MMMM")

    override val stateDefinition = CustomGlanceStateDefinition
    override val sizeMode: SizeMode = SizeMode.Responsive(
        setOf(TINY_BOX, ONE_ROW, HORIZONTAL_BOX)
    )

    companion object {
        val TINY_BOX = DpSize(160.dp, 120.dp)
        val ONE_ROW = DpSize(250.dp, 60.dp)
        val HORIZONTAL_BOX = DpSize(250.dp, 120.dp)
    }

    private val colorProviderSurface = ColorProvider(R.color.widget_surface)
    private val colorProviderOnSurface = ColorProvider(R.color.widget_on_surface)

    @Composable
    override fun Content() {

        //If the params needed in the widget grow, consider to serialize the object and parse it here.
        val state = currentState<Preferences>()

        val stateAphorism = Aphorism.fromDataStore(
            id = state[intPreferencesKey(Aphorism.PARAM_ID)],
            title = state[stringPreferencesKey(Aphorism.PARAM_TITLE)],
            dateInMillis = state[longPreferencesKey(Aphorism.PARAM_DATE)],
            summary = state[stringPreferencesKey(Aphorism.PARAM_TITLE)],
            link = state[stringPreferencesKey(Aphorism.PARAM_TITLE)],
        )

        if (stateAphorism.isValid) {

            when (WidgetSize.fromDpSize(LocalSize.current)) {

                WidgetSize.ROW -> RowWidgetView(aphorism = stateAphorism)
                WidgetSize.SMALL -> SmallWidgetView(aphorism = stateAphorism)
                else -> LargeWidgetView(aphorism = stateAphorism)
            }
        } else {
            EmptyView()
        }
    }

    @Composable
    fun RowWidgetView(aphorism: Aphorism) {

        val aphorismDate = aphorism.date ?: LocalDate.of(1923, 1, 7)
        val day = aphorismDate.dayOfMonth.toString()
        val month = aphorismDate.format(widgetDateFormatter)

        val boxModifier = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S)
            GlanceModifier
                .fillMaxSize()
                .background(ImageProvider(R.drawable.app_widget_background))
        else GlanceModifier
            .fillMaxSize()
            .background(colorProviderSurface)
            .cornerRadius(android.R.dimen.system_app_widget_background_radius)

        Box(
            modifier = boxModifier
                .padding(16.dp)
                .clickable(actionStartActivity<MainActivity>()),
            contentAlignment = Alignment.CenterEnd
        ) {

            Row(
                modifier = GlanceModifier.fillMaxSize(),
                horizontalAlignment = Alignment.Start,
                verticalAlignment = Alignment.CenterVertically
            ) {

                Column(
                    modifier = GlanceModifier
                    .padding(horizontal = 16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = day,
                        style = TextStyle(
                            color = ColorProvider(R.color.vpurple),
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold,
                        )
                    )

                    Text(
                        text = month,
                        style = TextStyle(
                            color = FixedColorProvider(md_theme_dark_onSurfaceVariant),
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Normal,
                        )
                    )
                }

                Column {
                    Text(
                        text = "Афоризъм на деня",
                        style = TextStyle(
                            fontSize = 10.sp,
                            color = FixedColorProvider(md_theme_dark_onSurfaceVariant),
                            fontStyle = FontStyle.Italic
                        )
                    )

                    Text(
                        modifier = GlanceModifier
                            .clickable(actionStartActivity<MainActivity>()),
                        text = aphorism.title,
                        style = TextStyle(
                            fontSize = 14.sp,
                            color = colorProviderOnSurface,
                            fontWeight = FontWeight.Medium,
                            textAlign = TextAlign.Left
                        )
                    )
                }
            }
        }
    }

    //NOTE: There's a bug in current Glance library version in which the elements of a inner view steal the focus of the container view.
    //so, in order to make the click work right now, we have to add the same clickable modifier to all the views. This will be fixed once the
    //new glance library version comes out (hopefully soon) check out this link: https://stackoverflow.com/questions/74471947/jetpack-glance-widget-only-the-whitespace-is-clickable-text-and-image-are-not

    // SMALL WIDGET
    @Composable
    fun SmallWidgetView(
        aphorism: Aphorism
    ) {
        val aphorismDate = aphorism.date ?: LocalDate.of(1923, 1, 7)
        val day = aphorismDate.dayOfMonth.toString()
        val month = aphorismDate.format(widgetDateFormatter)


        val boxModifier = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S)
            GlanceModifier
                .fillMaxSize()
                .background(ImageProvider(R.drawable.app_widget_background))
        else GlanceModifier
            .fillMaxSize()
            .background(colorProviderSurface)
            .cornerRadius(android.R.dimen.system_app_widget_background_radius)

        Box(
            modifier = boxModifier
                .padding(16.dp)
                .clickable(actionStartActivity<MainActivity>()),
            contentAlignment = Alignment.BottomStart
        ) {
            Image(
                provider = ImageProvider(R.drawable.snake_purple),
                contentDescription = "Snake",
                modifier = GlanceModifier
                    .fillMaxHeight()
                    .clickable(actionStartActivity<MainActivity>()),
            )

            Column(
                modifier = GlanceModifier.fillMaxHeight(),
                horizontalAlignment = Alignment.Start,
                verticalAlignment = Alignment.Bottom
            ) {

                DateRow(day = day, month = month)

                Spacer(
                    modifier = GlanceModifier
                        .defaultWeight()
                        .clickable(actionStartActivity<MainActivity>())
                )

                Text(
                    modifier = GlanceModifier
                        .clickable(actionStartActivity<MainActivity>()),
                    text = aphorism.title,
                    style = TextStyle(
                        fontSize = 18.sp,
                        color = colorProviderOnSurface,
                        fontWeight = FontWeight.Medium,
                        textAlign = TextAlign.Left
                    )
                )
            }
        }
    }


    // LARGE WIDGET
    @Composable
    fun LargeWidgetView(aphorism: Aphorism) {
        val aphorismDate = aphorism.date ?: LocalDate.of(1923, 1, 7)
        val day = aphorismDate.dayOfMonth.toString()
        val month = aphorismDate.format(widgetDateFormatter)

        val columnModifier = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S)
            GlanceModifier
                .fillMaxSize()
                .background(ImageProvider(R.drawable.app_widget_background))
        else GlanceModifier
            .fillMaxSize()
            .background(colorProviderSurface)
            .cornerRadius(android.R.dimen.system_app_widget_background_radius)

        Row(
            modifier = columnModifier
                .padding(16.dp)
                .clickable(actionStartActivity<MainActivity>()),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                provider = ImageProvider(
                    R.drawable.snake2
                ),
                contentDescription = "Змия на Мъдростта",
                modifier = GlanceModifier
                    .width(70.dp)
                    .fillMaxHeight()
//                        .padding(top = 24.dp)
                    .clickable(actionStartActivity<MainActivity>()),
            )

            Column(
                modifier = GlanceModifier
                    .fillMaxHeight()
                    .padding(start = 8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {

                Row(
                    modifier = GlanceModifier
                        .fillMaxWidth()
                        .padding(bottom = 5.dp),
                    verticalAlignment = Alignment.Bottom
                ) {

                    Text(
                        text = "Афоризъм на деня",
                        style = TextStyle(
                            fontSize = 12.sp,
                            color = FixedColorProvider(md_theme_dark_onSurfaceVariant),
                            fontStyle = FontStyle.Italic
                        )
                    )

//                    DateRow(day = day, month = month)

                }

                Text(
                    modifier = GlanceModifier
                        .clickable(actionStartActivity<MainActivity>())
                        .padding(bottom = 5.dp),
                    text = aphorism.title,
                    style = TextStyle(
                        fontSize = 18.sp,
                        color = colorProviderOnSurface,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Left
                    )
                )

                Text(
                    text = "Ваклуш",
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontStyle = FontStyle.Italic,
                        color = FixedColorProvider(md_theme_dark_onSurfaceVariant),
                        textAlign = TextAlign.Left
                    )
                )

            }
        }

    }

    @Composable
    fun EmptyView() {
        val boxModifier = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S)
            GlanceModifier
                .fillMaxSize()
                .background(ImageProvider(R.drawable.app_widget_background))
        else GlanceModifier
            .fillMaxSize()
            .background(colorProviderSurface)
            .cornerRadius(android.R.dimen.system_app_widget_background_radius)

        Box(
            modifier = boxModifier
                .padding(16.dp)
                .clickable(actionStartActivity<MainActivity>()),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "Натиснете този виз. елемент, за да заредите афоримза на деня.",
                style = TextStyle(colorProviderOnSurface)
            )
        }
    }

    @Composable
    fun DateRow(day: String, month: String) {
        Row(
            modifier = GlanceModifier
                .fillMaxWidth()
                .padding(bottom = 5.dp)
                .clickable(actionStartActivity<MainActivity>()),

            horizontalAlignment = Alignment.Start,
            verticalAlignment = Alignment.Bottom
        ) {

            Text(
                text = day,
                style = TextStyle(
                    color = ColorProvider(R.color.vpurple),
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Bold,
                ),
                modifier = GlanceModifier
                    .padding(end = 5.dp)
            )

            Text(
                text = month,
                style = TextStyle(
                    color = FixedColorProvider(md_theme_dark_onSurfaceVariant),
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Normal,
                )
            )
        }

    }

    enum class WidgetSize {
        SMALL, LARGE, ROW;

        companion object {
            fun fromDpSize(dpSize: DpSize): WidgetSize =
                when (dpSize.height) {
                    in 0.dp..ONE_ROW.height -> ROW
                    else -> when (dpSize.width) {
                        in 0.dp..TINY_BOX.width -> SMALL
                        else -> LARGE
                    }
                }
        }
    }
}