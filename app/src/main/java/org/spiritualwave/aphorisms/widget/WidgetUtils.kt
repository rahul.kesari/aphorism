package org.spiritualwave.aphorisms.widget

import android.content.Context
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.glance.appwidget.GlanceAppWidgetManager
import androidx.glance.appwidget.state.updateAppWidgetState
import androidx.glance.appwidget.updateAll
import org.spiritualwave.aphorisms.data.entities.Aphorism

object WidgetUtils {

    suspend fun updateGlanceWidget(context: Context, aphorism: Aphorism) {
        val glanceWidget = Widget()
        GlanceAppWidgetManager(context).getGlanceIds(Widget::class.java).forEach { glanceId ->
            updateAppWidgetState(context, glanceId) { prefs ->
                prefs[intPreferencesKey(Aphorism.PARAM_ID)] = aphorism.id
                prefs[stringPreferencesKey(Aphorism.PARAM_TITLE)] = aphorism.title
                prefs[longPreferencesKey(Aphorism.PARAM_DATE)] = aphorism.date?.toEpochDay() ?: 0L
                prefs[stringPreferencesKey(Aphorism.PARAM_LINK)] = aphorism.link
                prefs[stringPreferencesKey(Aphorism.PARAM_SUMMARY)] = aphorism.summary
            }
        }
        glanceWidget.updateAll(context)
    }
}