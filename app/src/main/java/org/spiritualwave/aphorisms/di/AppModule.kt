package org.spiritualwave.aphorisms.di

import android.content.Context
import android.util.Log
import androidx.room.Room
import org.spiritualwave.aphorisms.data.AphorismDAO
import org.spiritualwave.aphorisms.data.AphorismDatabase
import org.spiritualwave.aphorisms.data.AphorismRepository
import org.spiritualwave.aphorisms.data.UnsplashRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.json.Json
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideAphorismRepository(
        ktor: HttpClient,
        backgroundDispatcher: CoroutineDispatcher,
        dao: AphorismDAO
    ): AphorismRepository = AphorismRepository(ktor, backgroundDispatcher, dao)

    @Provides
    @Singleton
    fun provideUnsplashRepository(
        ktor: HttpClient,
        backgroundDispatcher: CoroutineDispatcher,
        dao: AphorismDAO
    ): UnsplashRepository = UnsplashRepository(ktor, backgroundDispatcher, dao)

    @Provides
    @Singleton
    fun provideKtorHttpClient(): HttpClient {
        return HttpClient(Android) {
            expectSuccess = true
            install(ContentNegotiation) {
                json(
                    Json {
                        prettyPrint = true
                        isLenient = true
                        ignoreUnknownKeys = true
                    }
                )
            }
            install(Logging) {
                logger = object : Logger {
                    override fun log(message: String) {
                        Log.v("http log: ", message)
                    }
                }
                level = LogLevel.ALL
            }

            install(DefaultRequest) {
                header(HttpHeaders.ContentType, ContentType.Application.Json)
            }

            install(HttpRequestRetry) {
                retryOnServerErrors(maxRetries = 5)
                exponentialDelay()
            }
        }
    }

    @Provides
    @Singleton
    fun providesBackgroundDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    @Provides
    @Singleton
    fun providesAphorismDatabase(@ApplicationContext context: Context): AphorismDatabase {
        return Room.databaseBuilder(
            context,
            AphorismDatabase::class.java,
            "aphorisms"
        )
            .createFromAsset("aphorisms.db")
            .build()
    }

    @Provides
    @Singleton
    fun providesAphorismDAO(db: AphorismDatabase): AphorismDAO {
        return db.aphorismDAO()
    }

}