package org.spiritualwave.aphorisms.ui.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.surfaceColorAtElevation
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun FavouriteAphorismsList(
    navController: NavController,
    aphorismsList: List<AphorismAndImage>
) {

    val groupedAphorisms = aphorismsList.groupBy { it.aphorism.title.first() }

    val listState = rememberLazyListState()
    val currentHeaderState = remember { mutableStateOf<Char?>(null) }

    LazyColumn(
        state = listState
    ) {
        groupedAphorisms.forEach { (header, aphorisms) ->

            stickyHeader {
                Surface(
                    modifier = Modifier
                        .fillParentMaxWidth()
                        .onGloballyPositioned {
                            if (it.positionInParent().y == 0.0F) {
                                currentHeaderState.value = header
                            }
                        },
                    color = if (header == currentHeaderState.value && listState.firstVisibleItemIndex > 0) MaterialTheme.colorScheme.surfaceColorAtElevation(12.dp) else MaterialTheme.colorScheme.background
                ) {
                    Text(
                        text = header.toString().replaceFirstChar { it.uppercase() },
                        style = MaterialTheme.typography.labelLarge,
                        modifier = Modifier
                            .padding(8.dp)
                    )
                }
            }

            items(aphorisms) { aphorism ->
                AphorismListItem(aphorism, navController, showDate = false)
            }
        }
    }

}