package org.spiritualwave.aphorisms.ui.util

import androidx.room.TypeConverter
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DateConverter {

    private val df: DateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE

    @TypeConverter
    fun dateToString(date: LocalDate?): String? {
        return date?.format(df)
    }

    @TypeConverter
    fun fromString(value: String?): LocalDate? {
        return if (value == "") {
            LocalDate.of(1900, 1, 1)
        } else {
            value?.let { LocalDate.parse(it, df) }
        }
    }


}