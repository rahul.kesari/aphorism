package org.spiritualwave.aphorisms.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.produceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.wajahatiqbal.blurhash.BlurHashPainter
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import org.spiritualwave.aphorisms.ui.components.AphorismContent
import org.spiritualwave.aphorisms.ui.components.AphorismTitle
import org.spiritualwave.aphorisms.ui.components.BackButtonTopBar
import org.spiritualwave.aphorisms.view_models.AphorismDetailsViewModel

@Composable
fun AphorismDetails(
    navController: NavController,
    aphorismId: Int,
    viewModel: AphorismDetailsViewModel = hiltViewModel()
) {
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp
    val screenWidth = configuration.screenWidthDp

    val aphorismDetails = produceState(initialValue = AphorismAndImage.SampleAphorism) {
        value = viewModel.getAphorismDetails(aphorismId = aphorismId)
    }.value

    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        AsyncImage(
            model = aphorismDetails.image?.url,
            contentDescription = "Image",
            fallback = painterResource(id = R.drawable.aphorism_image),
            placeholder = BlurHashPainter(
                blurHash = aphorismDetails.image?.blurHash,
                width = screenWidth,
                height = screenHeight
            ),
            contentScale = ContentScale.FillHeight,
            modifier = Modifier
                .fillMaxSize()
        )

        BackButtonTopBar(
            navController = navController,
            modifier = Modifier.fillMaxHeight(0.1f),
//            isFavourite = favStatus,
            markFavourite = {
                viewModel.toggleFavourite(aphorismDetails.aphorism)
            }
        )

        Column (
            modifier = Modifier
                .fillMaxHeight(0.8f)
                .fillMaxWidth()
                .clip(RoundedCornerShape(topStart = 28.dp, topEnd = 28.dp))
                .background(MaterialTheme.colorScheme.background.copy(alpha = 0.9f))
                .shadow(-(5).dp, shape = RoundedCornerShape(topStart = 28.dp, topEnd = 28.dp))
                .padding(horizontal = 25.dp)
                .align(Alignment.BottomCenter)
        ) {
            AphorismTitle(
                aphorism = aphorismDetails,
                modifier = Modifier
                    .padding(top = 20.dp)
            )
            AphorismContent(aphorism = aphorismDetails, navController = navController)
        }
    }
}

