package org.spiritualwave.aphorisms.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.wajahatiqbal.blurhash.BlurHashPainter
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import org.spiritualwave.aphorisms.ui.navigation.ScreenRoutes
import org.spiritualwave.aphorisms.ui.util.AutoScaledText
import java.time.format.DateTimeFormatter

@Composable
fun AphorismListItem(
    aphorism: AphorismAndImage,
    navController: NavController,
    modifier: Modifier = Modifier,
    showDate: Boolean = true
) {
    val dateFormatter = DateTimeFormatter.ofPattern("d MMM")
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp
    val screenWidth = configuration.screenWidthDp

    ListItem(
        headlineContent = {
            AutoScaledText(
                text = aphorism.aphorism.title.dropLastWhile { !it.isLetter() },
                style = MaterialTheme.typography.bodyLarge,
                modifier = modifier
                    .padding(end = 8.dp)
            )
        },
        leadingContent = {
            AsyncImage(
                model = aphorism.image?.thumb,
                contentDescription = "Image",
                fallback = painterResource(id = R.drawable.aphorism_image),
                placeholder = BlurHashPainter(
                    blurHash = aphorism.image?.blurHash,
                    width = screenWidth,
                    height = screenHeight
                ),
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .size(70.dp)
                    .clip(RoundedCornerShape(5.dp))
            )
        },
        trailingContent = {
            if (showDate) {
                aphorism.aphorism.date?.let {
                    Text(text = it.format(dateFormatter))
                }
            }
        },
        modifier = modifier
            .clickable {
                navController.navigate("${ScreenRoutes.AphorismDetails}/${aphorism.aphorism.id}")
            }
    )
}