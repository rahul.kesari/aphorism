package org.spiritualwave.aphorisms.ui.screens

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.wajahatiqbal.blurhash.BlurHashPainter
import kotlinx.coroutines.flow.collectLatest
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.ui.components.AphorismContent
import org.spiritualwave.aphorisms.ui.components.AphorismTitle
import org.spiritualwave.aphorisms.view_models.DailyAphorismViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DailyAphorism(
    viewModel: DailyAphorismViewModel = hiltViewModel(),
    scaffoldState: BottomSheetScaffoldState,
    navController: NavController
) {

    val dailyAphorism by viewModel.dailyAphorism.collectAsState()

    var sizeInDp by remember { mutableStateOf(0) }
    val density = LocalDensity.current
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp
    val screenWidth = configuration.screenWidthDp

    val snackBarHostState = remember { SnackbarHostState() }

    LaunchedEffect(Unit) {
        viewModel.isSnackBarShownFlow.collectLatest { show ->
            if (show) {
                snackBarHostState.showSnackbar(
                    message = "Заредени са ${viewModel.countNewAphorisms.value} нови афоризми.",
                    duration = SnackbarDuration.Long,
                    withDismissAction = true
                )
            }
        }
    }

    BottomSheetScaffold(
        scaffoldState = scaffoldState,
        sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp),
        sheetContainerColor = MaterialTheme.colorScheme.background.copy(alpha = 0.9f),
        sheetShadowElevation = 5.dp,
        sheetTonalElevation = 5.dp,
        sheetContent = {
            dailyAphorism?.let {
                Column (
                    modifier = Modifier
                        .padding(horizontal = 25.dp)
                        .heightIn(max = (screenHeight* 0.8).dp )
                        ) {
                    AphorismTitle(
                        aphorism = it,
                        modifier = Modifier
                            .onSizeChanged {

                            }
                            .fillMaxWidth()
                            .onGloballyPositioned {
                                sizeInDp = it.size.height
                            }
                    )
                    AphorismContent(aphorism = it, navController = navController)
                }
            }
        },
        sheetPeekHeight = (sizeInDp / density.density + 40).dp,
        snackbarHost = { SnackbarHost(snackBarHostState) }
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
        ) {
            AsyncImage(
                model = dailyAphorism?.image?.url,
                contentDescription = "Image",
                fallback = painterResource(id = R.drawable.aphorism_image),
                placeholder = BlurHashPainter(
                    blurHash = dailyAphorism?.image?.blurHash,
                    width = screenWidth,
                    height = screenHeight
                ),
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .fillMaxSize()
            )
        }
    }

}