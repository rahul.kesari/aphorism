package org.spiritualwave.aphorisms.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.DrawerState
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Text
import androidx.compose.material3.surfaceColorAtElevation
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.spiritualwave.aphorisms.BuildConfig
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.ui.navigation.MenuItems
import org.spiritualwave.aphorisms.ui.navigation.TopLevelDestination


@Composable
fun MenuSheet(
    scope: CoroutineScope,
    drawerState: DrawerState,
    selectedDestination: String,
    navigateTo: (TopLevelDestination) -> Unit
) {

    val menuItems = MenuItems
    val version = BuildConfig.VERSION_NAME

    ModalDrawerSheet (
        drawerContainerColor = MaterialTheme.colorScheme.surfaceColorAtElevation(12.dp)
    ) {

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(150.dp)
                .padding(horizontal = 16.dp, vertical = 30.dp),
            verticalAlignment = Alignment.Bottom,
            horizontalArrangement = Arrangement.Start
        ) {
            Image(
                painter = painterResource(id = R.drawable.vaklush_tolev),
                contentDescription = "Vaklush",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(width = 100.dp, height = 100.dp)
                    .clip(RoundedCornerShape(8.dp))
            )

            Spacer(modifier = Modifier.size(16.dp))

            Column(
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.Start
            ) {
                Text(
                    text = "Афоризми от",
                    color = MaterialTheme.colorScheme.onSurface
                )

                Text(
                    text = "Ваклуш Толев",
                    style = TextStyle(
                        color = MaterialTheme.colorScheme.onSurface,
                        fontSize = MaterialTheme.typography.headlineMedium.fontSize,
                        fontWeight = FontWeight.Medium
                    ),
                    overflow = TextOverflow.Visible,
                    softWrap = true
                )
            }
        }

        Spacer(Modifier.height(16.dp))

        menuItems.forEach { item ->
            NavigationDrawerItem(
                icon = {
                    Icon(
                        painter = item.iconSelected.asPainterResource(),
                        contentDescription = item.name,
                        modifier = Modifier.size(32.dp)
                    )
                },
                label = { Text(text = item.name) },
                selected = item.route == selectedDestination,
                onClick = {
                    scope.launch { drawerState.close() }
//                    navController.navigate(item.route)
                    navigateTo(item)
                },
                modifier = Modifier
                    .padding(8.dp)
            )

        }

        Spacer(modifier = Modifier.weight(1f))

        Text(
            text = version,
            fontSize = MaterialTheme.typography.labelSmall.fontSize,
            color = Color.DarkGray,
            modifier = Modifier
                .padding(horizontal = 8.dp)
        )


    }
}