package org.spiritualwave.aphorisms.ui.components

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import org.spiritualwave.aphorisms.ui.navigation.ScreenRoutes
import org.spiritualwave.aphorisms.ui.util.AutoScaledText
import java.time.format.DateTimeFormatter

@Composable
fun AphorismContent(
    modifier: Modifier = Modifier,
    navController: NavController,
    aphorism: AphorismAndImage,
) {
    val scrollState = rememberScrollState()
    val dateFormatter = DateTimeFormatter.ofPattern("d MMMM yyyy")
    val ctx = LocalContext.current
    var showShareMenu by remember { mutableStateOf(false) }

    Column(
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {

        // Aphorism date
        aphorism.aphorism.date?.let {
            Text(
                text = it.format(dateFormatter),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.bodySmall,
                modifier = Modifier
                    .padding(bottom = 16.dp)
            )
        }

        // Summary
        Text(
            text = aphorism.aphorism.summary,
            style = MaterialTheme.typography.bodyLarge,
            modifier = Modifier
                .verticalScroll(scrollState)
                .weight(5f)
        )

        // Unsplash credits
        aphorism.image?.let { img ->
            Row (
                horizontalArrangement = Arrangement.Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 12.dp)
            ) {
                UnsplashCreditsText(
                    user = img.user,
                    userUrl = "https://unsplash.com/@${img.username}?utm_source=aphorism_of_the_day&utm_medium=referral"
                )
            }
        }

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 16.dp)
        ) {
            val sendTextIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, aphorism.aphorism.title + " (Ваклуш)")
                type = "text/plain"
            }
            val shareTextIntent = Intent.createChooser(sendTextIntent, "Сподели афоризъм")

            FilledTonalButton(
                onClick = { showShareMenu = true },
                contentPadding = ButtonDefaults.ButtonWithIconContentPadding
            ) {
                Icon(
                    Icons.Default.Share,
                    contentDescription = "Сподели",
                    tint = MaterialTheme.colorScheme.onSurfaceVariant,
                    modifier = Modifier.size(ButtonDefaults.IconSize)
                )
                Spacer(Modifier.size(ButtonDefaults.IconSpacing))
                AutoScaledText(text = "Сподели")
            }

            // Share menu
            DropdownMenu(
                expanded = showShareMenu,
                onDismissRequest = { showShareMenu = false }
            ) {
                DropdownMenuItem(
                    text = { Text("Като текст") },
                    leadingIcon = {
                        Icon(
                            painter = painterResource(id = R.drawable.notes),
                            contentDescription = "Share as text",
                            modifier = Modifier
                                .size(24.dp)
                        )
                    },
                    onClick = { ctx.startActivity(shareTextIntent) }
                )

                DropdownMenuItem(
                    text = { Text("Като изображение") },
                    leadingIcon = {
                        Icon(
                            painter = painterResource(id = R.drawable.image),
                            contentDescription = "Share as image",
                            modifier = Modifier
                                .size(24.dp)
                        )
                    },
                    onClick = {
                        navController.navigate("${ScreenRoutes.ShareScreen}/${aphorism.aphorism.id}")
                        showShareMenu = false
                    }
                )
            }

            Button(
                onClick = {
                    val urlIntent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(aphorism.aphorism.link)
                    )
                    ctx.startActivity(urlIntent)
                },
                contentPadding = ButtonDefaults.ButtonWithIconContentPadding
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.read),
                    contentDescription = "Прочети още",
                    modifier = Modifier.size(ButtonDefaults.IconSize)
                )
                Spacer(Modifier.size(ButtonDefaults.IconSpacing))
                AutoScaledText(text = "Прочети още")
            }
        }

    }

}

@Composable
fun UnsplashCreditsText(
    user: String,
    userUrl: String
) {
    val ctx = LocalContext.current
    val annotatedText = buildAnnotatedString {
        withStyle(style = SpanStyle(color = MaterialTheme.colorScheme.onSurfaceVariant, fontStyle = FontStyle.Italic)) {
            append("Снимка: ")
        }
        withStyle(style = SpanStyle(color = Color.Gray, fontWeight = FontWeight.Bold)) {
            appendLink(user, userUrl)
        }
        withStyle(style = SpanStyle(color = MaterialTheme.colorScheme.onSurfaceVariant)) {
            append(" от ")
        }
        withStyle(style = SpanStyle(color = Color.Gray, fontWeight = FontWeight.Bold)) {
            appendLink("Unsplash", "https://unsplash.com/")
        }
    }

    ClickableText(
        text = annotatedText,
        style = MaterialTheme.typography.labelSmall,
        onClick = { offset ->
            annotatedText.onLinkClick(offset) { link ->
                val urlIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(link)
                )
                ctx.startActivity(urlIntent)
                println("Clicked URL: $link")
            }
        }
    )
}


fun AnnotatedString.Builder.appendLink(linkText: String, linkUrl: String) {
    pushStringAnnotation(tag = linkUrl, annotation = linkUrl)
    append(linkText)
    pop()
}

fun AnnotatedString.onLinkClick(offset: Int, onClick: (String) -> Unit) {
    getStringAnnotations(start = offset, end = offset).firstOrNull()?.let {
        onClick(it.item)
    }
}