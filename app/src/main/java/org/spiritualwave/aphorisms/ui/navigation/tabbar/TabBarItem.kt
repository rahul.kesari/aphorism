package org.spiritualwave.aphorisms.ui.navigation.tabbar


import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.ui.navigation.ScreenRoutes
import org.spiritualwave.aphorisms.ui.navigation.TopLevelDestination
import org.spiritualwave.aphorisms.ui.util.IconResource

//data class TabBarItem(
//    val name: String,
//    val route: String,
//    val iconSelected: IconResource,
//    val iconUnselected: IconResource
//)

val MainScreens = listOf(
    TopLevelDestination(
        name = "Предишни",
        route = ScreenRoutes.PreviousAphorisms,
        iconSelected = IconResource.fromDrawableResource(R.drawable.previous),
        iconUnselected = IconResource.fromDrawableResource(R.drawable.previous)
    ),
    TopLevelDestination(
        name = "Афоризъм на деня",
        route = ScreenRoutes.DailyAphorism,
        iconSelected = IconResource.fromDrawableResource(R.drawable.today_fill),
        iconUnselected = IconResource.fromDrawableResource(R.drawable.today)
    ),
    TopLevelDestination(
        name = "Любими",
        route = ScreenRoutes.FavouriteAphorisms,
        iconSelected = IconResource.fromDrawableResource(R.drawable.favorite_fill),
        iconUnselected = IconResource.fromDrawableResource(R.drawable.favorite)
    )
)

