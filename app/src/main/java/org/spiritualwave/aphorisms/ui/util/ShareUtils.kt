package org.spiritualwave.aphorisms.ui.util

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.core.content.FileProvider
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class ShareUtils {

    companion object{

        private fun saveBitmapAndGetUri(context: Context, bitmap: Bitmap, fileName: String): Uri? {
            val path: String = context.externalCacheDir.toString() + "/$fileName.jpg"
            val out: OutputStream?
            val file = File(path)
            try {
                Log.d("VAKLUSH", "Share util path: $path")
                out = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return FileProvider.getUriForFile(
                context, context.packageName + ".org.spiritualwave.aphorisms.provider", file
            )
        }

        fun shareImageToOthers(context: Context, text: String?, bitmap: Bitmap?, fileName: String) {
            val imageUri: Uri? = bitmap?.let { saveBitmapAndGetUri(context, it, fileName) }
            val chooserIntent = Intent(Intent.ACTION_SEND)
            chooserIntent.type = "image/jpg"
            chooserIntent.putExtra(Intent.EXTRA_TEXT, text)
            chooserIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            chooserIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            try {
                context.startActivity(chooserIntent)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }
}