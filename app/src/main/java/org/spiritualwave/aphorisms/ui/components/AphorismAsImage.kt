package org.spiritualwave.aphorisms.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.produceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import org.spiritualwave.aphorisms.view_models.AphorismDetailsViewModel

@Composable
fun AphorismAsImage(
    modifier: Modifier = Modifier,
    viewModel: AphorismDetailsViewModel = hiltViewModel(),
    aphorismId: Int
) {
    val aphorismDetails = produceState(initialValue = AphorismAndImage.SampleAphorism) {
        value = viewModel.getAphorismDetails(aphorismId = aphorismId)
    }.value

    Box(
        modifier = modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center
    ) {
        AsyncImage(
            model = aphorismDetails.image?.url,
            contentDescription = "Image",
            fallback = painterResource(id = R.drawable.aphorism_image),
            contentScale = ContentScale.Crop,
            modifier = modifier
                .fillMaxSize()
        )

        Box(
            modifier = modifier
                .fillMaxSize(),
            contentAlignment = Alignment.BottomStart
        ) {
            aphorismDetails.image?.let { img ->
                Row(
                    horizontalArrangement = Arrangement.Start,
                    modifier = modifier
                        .background(Color.Black.copy(alpha = 0.8f))
                        .padding(vertical = 3.dp, horizontal = 10.dp)
                ) {
                    Text(
                        text = "Снимка: ${img.user} от Unsplash.com",
                        style = TextStyle(
                            color = Color.White.copy(alpha = 0.3f),
                            fontStyle = FontStyle.Italic,
                            fontSize = MaterialTheme.typography.labelSmall.fontSize
                        )
                    )
                }
            }
        }

        Box(
            modifier = modifier
                .padding(30.dp)
                .background(
                    color = MaterialTheme.colorScheme.background.copy(alpha = 0.7f),
                    shape = RoundedCornerShape(16.dp)
                )
                .align(Alignment.Center)
        ) {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = modifier
                    .padding(20.dp)
            ) {

                Text(
                    text = "Афоризъм на деня",
                    style = TextStyle(
                        color = Color.Gray,
                        fontStyle = FontStyle.Italic
                    )
                )

                AphorismTitle(
                    aphorism = aphorismDetails,
                    modifier = modifier
                        .padding(top = 20.dp)
                )

                Text(
                    text = "Ваклуш",
                    style = TextStyle(
                        fontStyle = FontStyle.Italic
                    )
                )
            }
        }
    }
}
