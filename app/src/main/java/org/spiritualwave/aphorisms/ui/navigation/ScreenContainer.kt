package org.spiritualwave.aphorisms.ui.navigation

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.BottomSheetScaffoldState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import org.spiritualwave.aphorisms.ui.screens.AboutVaklush
import org.spiritualwave.aphorisms.ui.screens.AphorismDetails
import org.spiritualwave.aphorisms.ui.screens.DailyAphorism
import org.spiritualwave.aphorisms.ui.screens.FavouriteAphorisms
import org.spiritualwave.aphorisms.ui.screens.PreviousAphorisms
import org.spiritualwave.aphorisms.ui.screens.SettingsScreen
import org.spiritualwave.aphorisms.view_models.SearchAppBarViewModel
import org.spiritualwave.aphorisms.ui.screens.ShareScreen



@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterial3Api::class)
@Composable
fun ScreenContainer(
    navController: NavHostController,
    innerPaddingValues: PaddingValues,
    searchAppBarViewModel: SearchAppBarViewModel,
    scaffoldState: BottomSheetScaffoldState
) {

    AnimatedNavHost(
        navController = navController,
        startDestination = ScreenRoutes.DailyAphorism,
        enterTransition = {
            slideIntoContainer(
                towards = AnimatedContentTransitionScope.SlideDirection.Start,
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioLowBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
        },
        exitTransition = {
            slideOutOfContainer(
                towards = AnimatedContentTransitionScope.SlideDirection.End,
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioLowBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
        },
        modifier = Modifier
            .padding(innerPaddingValues)
    ) {

        // PREVIOUS APHORISMS
        composable(
            route = ScreenRoutes.PreviousAphorisms,
            enterTransition = {
                slideIntoContainer(
                    towards = AnimatedContentTransitionScope.SlideDirection.End,
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioLowBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            },
            exitTransition = {
                slideOutOfContainer(
                    towards = AnimatedContentTransitionScope.SlideDirection.Start,
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioLowBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            }
        ) {
            PreviousAphorisms(
                navController = navController,
                searchAppBarViewModel = searchAppBarViewModel
            )
        }

        // DAILY APHORISM
        composable(
            route = ScreenRoutes.DailyAphorism,
            enterTransition = {
                  when (initialState.destination.route) {
                      ScreenRoutes.PreviousAphorisms ->
                          slideIntoContainer(
                              towards = AnimatedContentTransitionScope.SlideDirection.Start,
                              animationSpec = spring(
                                  dampingRatio = Spring.DampingRatioLowBouncy,
                                  stiffness = Spring.StiffnessLow
                              )
                          )
                      ScreenRoutes.FavouriteAphorisms ->
                          slideIntoContainer(
                              towards = AnimatedContentTransitionScope.SlideDirection.End,
                              animationSpec = spring(
                                  dampingRatio = Spring.DampingRatioLowBouncy,
                                  stiffness = Spring.StiffnessLow
                              )
                          )
                      else -> null
                  }
            },
            exitTransition = {
                when (targetState.destination.route) {
                    ScreenRoutes.PreviousAphorisms ->
                        slideOutOfContainer(
                            towards = AnimatedContentTransitionScope.SlideDirection.End,
                            animationSpec = spring(
                                dampingRatio = Spring.DampingRatioLowBouncy,
                                stiffness = Spring.StiffnessLow
                            )
                        )
                    ScreenRoutes.FavouriteAphorisms ->
                        slideOutOfContainer(
                            towards = AnimatedContentTransitionScope.SlideDirection.Start,
                            animationSpec = spring(
                                dampingRatio = Spring.DampingRatioLowBouncy,
                                stiffness = Spring.StiffnessLow
                            )
                        )
                    else -> null
                }
            }
        ) {
            DailyAphorism(
                scaffoldState = scaffoldState,
                navController = navController
            )
        }

        // FAVOURITE APHORISMS
        composable(
            route = ScreenRoutes.FavouriteAphorisms,
            enterTransition = null,
            exitTransition = null
        ) {
            FavouriteAphorisms(navController, searchAppBarViewModel)
        }

        // APHORISM DETAILS
        composable(
            route = "${ScreenRoutes.AphorismDetails}/{aphorismId}",
            arguments = listOf(
                navArgument("aphorismId") { type = NavType.IntType}
            ),
            enterTransition = {
                slideIntoContainer(
                    AnimatedContentTransitionScope.SlideDirection.Left,
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioLowBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            },
            exitTransition = {
                slideOutOfContainer(
                    AnimatedContentTransitionScope.SlideDirection.Right,
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioLowBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            }
        ) {
            val aphorismId = remember {
                it.arguments?.getInt("aphorismId") ?: 1
            }
            AphorismDetails(
                navController = navController,
                aphorismId = aphorismId
            )
        }

        // ABOUT VAKLUSH
        composable(
            route = ScreenRoutes.AboutVaklush
        ) {
            AboutVaklush()
        }

        // ABOUT APHORISMS
        composable(
            route = ScreenRoutes.AboutAphorisms
        ) {
            AboutVaklush()
        }

        // SHARE SCREEN
        composable(
            route = "${ScreenRoutes.ShareScreen}/{aphorismId}",
            arguments = listOf(
                navArgument("aphorismId") { type = NavType.IntType}
            )
        ) {
            val aphorismId = remember { it.arguments?.getInt("aphorismId") ?: 1 }
            ShareScreen(navController = navController, aphorismId = aphorismId)
        }

        // SETTINGS SCREEN
        composable(ScreenRoutes.SettingsScreen) {
            SettingsScreen(navController)
        }

    }
}