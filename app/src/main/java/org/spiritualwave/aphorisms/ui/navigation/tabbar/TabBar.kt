package org.spiritualwave.aphorisms.ui.navigation.tabbar

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.compose.currentBackStackEntryAsState
import org.spiritualwave.aphorisms.ui.navigation.TopLevelDestination
import org.spiritualwave.aphorisms.ui.util.AutoScaledText

@Composable
fun TabBar(
    navController: NavController,
    onItemClick: (TopLevelDestination) -> Unit
) {
    val backStackEntry = navController.currentBackStackEntryAsState()

    NavigationBar {
        MainScreens.forEach { item ->

            val selected = item.route == backStackEntry.value?.destination?.route

            NavigationBarItem(
                selected = selected,
                onClick = {
                    if (selected) Unit else onItemClick(item)
                },
                icon = {
                    Column(horizontalAlignment = CenterHorizontally) {
                        Icon(
                            if (selected) item.iconSelected.asPainterResource() else item.iconUnselected.asPainterResource(),
                            contentDescription = item.name,
                            tint = if (selected) MaterialTheme.colorScheme.primary else Color.Gray
                        )
                        AutoScaledText(
                            text = item.name,
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.labelSmall,
                            color = if (selected) MaterialTheme.colorScheme.primary else Color.Gray,
                            fontWeight = if (selected) FontWeight.Medium else FontWeight.Normal
                        )
                    }
                }
            )
        }
    }
}