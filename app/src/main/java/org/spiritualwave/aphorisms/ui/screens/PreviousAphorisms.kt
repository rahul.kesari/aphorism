package org.spiritualwave.aphorisms.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import kotlinx.coroutines.launch
import org.spiritualwave.aphorisms.ui.components.MainAppBar
import org.spiritualwave.aphorisms.ui.components.MenuSheet
import org.spiritualwave.aphorisms.ui.components.PreviousAphorismsList
import org.spiritualwave.aphorisms.ui.navigation.NavigationActions
import org.spiritualwave.aphorisms.ui.navigation.ScreenRoutes
import org.spiritualwave.aphorisms.ui.util.SearchWidgetState
import org.spiritualwave.aphorisms.view_models.PreviousAphorismsViewModel
import org.spiritualwave.aphorisms.view_models.SearchAppBarViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PreviousAphorisms(
    navController: NavController,
    searchAppBarViewModel: SearchAppBarViewModel,
    viewModel: PreviousAphorismsViewModel = hiltViewModel()
) {

    val aphorismsList by viewModel.previousAphorisms.collectAsState()
    val searchWidgetState by searchAppBarViewModel.searchWidgetState
    val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val selectedDestination = navBackStackEntry?.destination?.route ?: ScreenRoutes.PreviousAphorisms
    val navigationActions = remember(navController) {
        NavigationActions(navController)
    }

    ModalNavigationDrawer(
        drawerState = drawerState,
        gesturesEnabled = true,
        drawerContent = {
            MenuSheet(
                scope = scope,
                drawerState = drawerState,
                selectedDestination = selectedDestination,
                navigateTo = navigationActions::navigateTo
            )
        }
    ) {
        Scaffold(
            modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
            topBar = {
                MainAppBar(
                    titleText = "Предишни афоризми",
                    searchWidgetState = searchWidgetState,
                    onCloseClicked = {
                        searchAppBarViewModel.updateSearchWidgetState(newValue = SearchWidgetState.CLOSED)
                    },
                    onSearchTriggered = {
                        searchAppBarViewModel.updateSearchWidgetState(newValue = SearchWidgetState.OPENED)
                    },
                    onMenuClicked = {
                        scope.launch {
                            drawerState.open()
                        }
                    },
                    scrollBehavior = scrollBehavior
                )
            },
            content = { innerPadding ->
                val ld: LayoutDirection = LayoutDirection.Ltr
                Column(
                    modifier = Modifier
                        .padding(
                            start = innerPadding.calculateStartPadding(ld),
                            top = innerPadding.calculateTopPadding(),
                            end = innerPadding.calculateEndPadding(ld),
                            bottom = 0.dp
                        )
                ) {
                    if (aphorismsList.isEmpty()) {
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(16.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = "От утре нататък тук ще виждате афоризмите от предишните дни.",
                                textAlign = TextAlign.Center,
                                color = Color.Gray
                            )
                        }
                    } else {
                        PreviousAphorismsList(navController, aphorismsList)
                    }
                }
            }
        )
    }



}