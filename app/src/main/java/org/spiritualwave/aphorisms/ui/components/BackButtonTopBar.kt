package org.spiritualwave.aphorisms.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.view_models.AphorismDetailsViewModel

@Composable
fun BackButtonTopBar(
    modifier: Modifier = Modifier,
    navController: NavController,
    viewModel: AphorismDetailsViewModel = hiltViewModel(),
    markFavourite: () -> Unit
) {

    val favStatus by viewModel.isFavourite.collectAsState()

    Box(
        contentAlignment = Alignment.TopStart,
        modifier = modifier
            .fillMaxWidth()
            .background(
                Brush.verticalGradient(listOf(Color.Black.copy(alpha = 0.1f), Color.Transparent))
            )
            .padding(top = 20.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Icon(
                painter = painterResource(id = R.drawable.chevron_backward),
                contentDescription = "Обратно",
                tint = MaterialTheme.colorScheme.background,
                modifier = Modifier
                    .size(25.dp)
                    .offset(15.dp, 20.dp)
                    .clickable { navController.popBackStack() }
            )

            Icon(
                painter = painterResource(id = if (favStatus) R.drawable.favorite_fill else R.drawable.favorite),
                contentDescription = "Отбележи като любим",
                tint = MaterialTheme.colorScheme.background,
                modifier = Modifier
                    .size(25.dp)
                    .offset(-(20).dp, 20.dp)
                    .clickable { markFavourite() }
            )
        }
    }
}