package org.spiritualwave.aphorisms.ui.screens

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.surfaceColorAtElevation
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asAndroidBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import dev.shreyaspatil.capturable.Capturable
import dev.shreyaspatil.capturable.controller.rememberCaptureController
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.ui.components.AphorismAsImage
import org.spiritualwave.aphorisms.ui.util.ShareUtils

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ShareScreen(navController: NavController, aphorismId: Int) {

    val context = LocalContext.current
    val controller = rememberCaptureController()
    var aphorismImageBitmap: ImageBitmap? by remember { mutableStateOf(null) }

    Scaffold(
        modifier = Modifier
            .fillMaxSize(),
        topBar = {
                 TopAppBar(
                     title = { Text(text = "Сподели като изображение") },
                     navigationIcon = {
                         IconButton(
                             onClick = { navController.popBackStack() }
                         ) { Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "Back")}
                      },
                     colors = TopAppBarDefaults.topAppBarColors(
                         containerColor = MaterialTheme.colorScheme.surfaceColorAtElevation(12.dp)
                     )
                 )
        }
    ) {
        Column(
            modifier = Modifier
                .padding(it)
                .padding(horizontal = 20.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {

            Capturable(
                controller = controller,
                modifier = Modifier
                    .requiredSize(400.dp, 500.dp)
                    .padding(10.dp),
                onCaptured = { bitmap, error ->
                    aphorismImageBitmap = bitmap

                    aphorismImageBitmap?.let { imgbmp ->
                        val aphorismBitmap = imgbmp.asAndroidBitmap()
                        val fileName = "aphorism-$aphorismId"

                        ShareUtils.shareImageToOthers(context, "Афоризъм на деня от Ваклуш", aphorismBitmap, fileName)
                    }
                    Log.d("VAKLUSH", "Capturable error: $error")
                }
            ) {
                AphorismAsImage(aphorismId = aphorismId)
            }

            Spacer(modifier = Modifier.size(32.dp))

            ElevatedButton(
                onClick = { controller.capture() },
                shape = RoundedCornerShape(16.dp),
                elevation = ButtonDefaults.elevatedButtonElevation(),
                colors = ButtonDefaults.elevatedButtonColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    contentColor = MaterialTheme.colorScheme.onPrimary
                ),
                contentPadding = PaddingValues(horizontal = 40.dp, vertical = 15.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.image),
                    contentDescription = "Image",
                    modifier = Modifier.size(ButtonDefaults.IconSize)
                )
                Text(text = "Сподели", modifier = Modifier.padding(horizontal = 8.dp))
            }

        }
    }
}