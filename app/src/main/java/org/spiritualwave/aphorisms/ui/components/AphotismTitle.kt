package org.spiritualwave.aphorisms.ui.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import org.spiritualwave.aphorisms.data.relations.AphorismAndImage
import org.spiritualwave.aphorisms.ui.util.AutoScaledText

@Composable
fun AphorismTitle(
    aphorism: AphorismAndImage,
    modifier: Modifier = Modifier
) {
    AutoScaledText(
        text = aphorism.aphorism.title.dropLastWhile { !it.isLetter() },
        style = MaterialTheme.typography.headlineMedium,
        textAlign = TextAlign.Center,
        fontWeight = FontWeight.Medium,
        modifier = modifier
            .padding(bottom = 16.dp)
    )
}