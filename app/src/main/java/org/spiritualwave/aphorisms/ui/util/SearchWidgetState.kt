package org.spiritualwave.aphorisms.ui.util

enum class SearchWidgetState {
    OPENED,
    CLOSED
}