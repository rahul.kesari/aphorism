package org.spiritualwave.aphorisms.ui.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import org.spiritualwave.aphorisms.R
import org.spiritualwave.aphorisms.ui.util.IconResource

object ScreenRoutes {
    const val DailyAphorism = "daily_aphorism"
    const val PreviousAphorisms = "previous_aphorisms"
    const val FavouriteAphorisms = "favourite_aphorisms"
    const val AphorismDetails = "aphorism_details"
    const val AboutAphorisms = "about_aphorisms"
    const val AboutVaklush = "about_vaklush"
    const val ShareScreen = "share_screen"
    const val SettingsScreen = "settings_screen"
}

data class TopLevelDestination (
    val name: String,
    val route: String,
    val iconSelected: IconResource,
    val iconUnselected: IconResource
)

val MenuItems = listOf(

    // За Ваклуш
    TopLevelDestination(
        name = "Ваклуш",
        iconSelected = IconResource.fromDrawableResource(R.drawable.person),
        iconUnselected = IconResource.fromDrawableResource(R.drawable.person),
        route = ScreenRoutes.AboutVaklush
    ),

    // За афоризмите
    TopLevelDestination(
        name = "За афоризмите",
        iconSelected = IconResource.fromDrawableResource(R.drawable.book),
        iconUnselected = IconResource.fromDrawableResource(R.drawable.book),
        route = ScreenRoutes.AboutAphorisms
    ),

    // Settings
    TopLevelDestination(
        name = "Настройки",
        iconSelected = IconResource.fromDrawableResource(R.drawable.settings),
        iconUnselected = IconResource.fromDrawableResource(R.drawable.settings),
        route = ScreenRoutes.SettingsScreen
    )
)

class NavigationActions(private val navController: NavController) {

    fun navigateTo(destination: TopLevelDestination) {
        navController.navigate(destination.route) {
            // Pop up to the start destination of the graph to
            // avoid building up a large stack of destinations
            // on the back stack as users select items
            popUpTo(navController.graph.findStartDestination().id) {
                saveState = true
            }
            // Avoid multiple copies of the same destination when
            // reselecting the same item
            launchSingleTop = true
            // Restore state when reselecting a previously selected item
            restoreState = true
        }
    }
}