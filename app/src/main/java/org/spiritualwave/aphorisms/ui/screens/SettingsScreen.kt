package org.spiritualwave.aphorisms.ui.screens

import android.app.TimePickerDialog
import android.content.Context
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.surfaceColorAtElevation
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import org.spiritualwave.aphorisms.view_models.SettingsViewModel
import java.util.Calendar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(
    navController: NavController,
    viewModel: SettingsViewModel = hiltViewModel()
) {

    Scaffold(
        modifier = Modifier
            .fillMaxSize(),
        topBar = {
            TopAppBar(
                title = { Text(text = "Настройки") },
                navigationIcon = {
                    IconButton(
                        onClick = { navController.popBackStack() }
                    ) { Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "Back") }
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.surfaceColorAtElevation(12.dp)
                )
            )
        }
    ) {
        Column(
            modifier = Modifier
                .padding(it)
                .padding(horizontal = 20.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            val context = LocalContext.current
            val selectedTime = viewModel.notificationTime.collectAsState()
            Text(text = selectedTime.value)
            Button(
                onClick = {
                    showTimePicker(context) { time ->
                        viewModel.enableAndSaveNotificationTime(time)
                    }
                }) {
                Text(text = "Enable Notification")
            }
            Button(
                onClick = {
                    viewModel.disableNotification()
                }) {
                Text(text = "Disable Notification")
            }
        }
    }
    showBatteryOptimizationAlert(viewModel = viewModel)
}

@Composable
fun showBatteryOptimizationAlert(viewModel: SettingsViewModel) {
    val showDialog = viewModel.showBatteryOptimizationAlert.collectAsState()

    if (showDialog.value) {
        AlertDialog(
            onDismissRequest = { viewModel.handleAlertVisibility(false) },
            title = {
                Text("Battery Optimization")
            },
            text = {
                Text("To get the Notification please disable the Battery optimization \n Go to Battery > Unrestricted")
            },
            confirmButton = {
                Button(onClick = {
                    viewModel.handleAlertVisibility(false)
                    viewModel.showBatteryOptimizationSettings()
                }) {
                    Text("OK")
                }
            },
            dismissButton = {
                Button(onClick = { viewModel.handleAlertVisibility(false) }) {
                    Text("Cancel")
                }
            }
        )
    }
}

fun showTimePicker(context: Context, selectedTime: (String) -> Unit) {
    val calendar = Calendar.getInstance()
    val currentHour = calendar.get(Calendar.HOUR_OF_DAY)
    val currentMinute = calendar.get(Calendar.MINUTE)

    val timePickerDialog = TimePickerDialog(
        context,
        { _, hour, minute ->
            selectedTime.invoke("$hour:$minute")
        },
        currentHour,
        currentMinute,
        false
    )

    timePickerDialog.show()
}