package org.spiritualwave.aphorisms.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import kotlinx.coroutines.launch
import org.spiritualwave.aphorisms.ui.components.FavouriteAphorismsList
import org.spiritualwave.aphorisms.ui.components.MainAppBar
import org.spiritualwave.aphorisms.ui.components.MenuSheet
import org.spiritualwave.aphorisms.ui.navigation.NavigationActions
import org.spiritualwave.aphorisms.ui.navigation.ScreenRoutes
import org.spiritualwave.aphorisms.ui.util.SearchWidgetState
import org.spiritualwave.aphorisms.view_models.FavouriteAphorismsViewModel
import org.spiritualwave.aphorisms.view_models.SearchAppBarViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FavouriteAphorisms(
    navController: NavController,
    searchAppBarViewModel: SearchAppBarViewModel,
    viewModel: FavouriteAphorismsViewModel = hiltViewModel()
) {

    val aphorismsList by viewModel.favouriteAphorisms.collectAsState()
    val searchWidgetState by searchAppBarViewModel.searchWidgetState
    val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val selectedDestination = navBackStackEntry?.destination?.route ?: ScreenRoutes.PreviousAphorisms
    val navigationActions = remember(navController) {
        NavigationActions(navController)
    }

    ModalNavigationDrawer(
        drawerState = drawerState,
        gesturesEnabled = true,
        drawerContent = {
            MenuSheet(
                scope = scope,
                drawerState = drawerState,
                selectedDestination = selectedDestination,
                navigateTo = navigationActions::navigateTo
            )
        }
    ) {
        Scaffold(
            modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
            topBar = {
                MainAppBar(
                    titleText = "Любими афоризми",
                    searchWidgetState = searchWidgetState,
                    onCloseClicked = {
                        searchAppBarViewModel.updateSearchWidgetState(newValue = SearchWidgetState.CLOSED)
                    },
                    onSearchTriggered = {
                        searchAppBarViewModel.updateSearchWidgetState(newValue = SearchWidgetState.OPENED)
                    },
                    onMenuClicked = {
                        scope.launch {
                            drawerState.open()
                        }
                    },
                    scrollBehavior = scrollBehavior
                )
            },
            content = { innerPadding ->
                Column(
                    modifier = Modifier
                        .padding(innerPadding)
                ) {
                    if (aphorismsList.isEmpty()) {
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(16.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = "За да отбележите афоризъм като любим, отворете го от списъка в Предишни и натиснете на иконата със сърцето в горния десен ъгъл.",
                                textAlign = TextAlign.Center,
                                color = Color.Gray
                            )
                        }
                    } else {
                        FavouriteAphorismsList(
                            navController = navController,
                            aphorismsList = aphorismsList
                        )
                    }
                }
            }
        )
    }
}