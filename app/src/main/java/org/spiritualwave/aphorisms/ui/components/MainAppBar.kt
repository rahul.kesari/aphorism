package org.spiritualwave.aphorisms.ui.components

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.togetherWith
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.IntSize
import org.spiritualwave.aphorisms.ui.util.SearchWidgetState

@OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)
@Composable
fun MainAppBar(
    searchWidgetState: SearchWidgetState,
    onCloseClicked: () -> Unit,
    onSearchTriggered: () -> Unit,
    onMenuClicked: () -> Unit,
    titleText: String,
    scrollBehavior: TopAppBarScrollBehavior
) {
    AnimatedContent(
        targetState = searchWidgetState,
        transitionSpec = {
            fadeIn(animationSpec = tween(300, 150)) togetherWith
                    fadeOut(animationSpec = tween(300)) using
                    SizeTransform { initialSize, targetSize ->
                        if (targetState == SearchWidgetState.CLOSED) {
                            keyframes {
                                // Expand horizontally first.
                                IntSize(targetSize.width, initialSize.height) at 150
                                durationMillis = 300
                            }
                        } else {
                            keyframes {
                                // Shrink vertically first.
                                IntSize(initialSize.width, targetSize.height) at 150
                                durationMillis = 300
                            }
                        }
                    }
        }
    ) { targetState ->
        when (targetState) {
            SearchWidgetState.CLOSED -> {
                DefaultAppBar(
                    titleText = titleText,
                    onSearchClicked = onSearchTriggered,
                    onMenuClicked = onMenuClicked,
                    scrollBehavior = scrollBehavior
                )
            }
            SearchWidgetState.OPENED -> {
                SearchAppBar(
                    onCloseClicked = onCloseClicked
                )
            }
        }
    }

}