package org.spiritualwave.aphorisms.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import org.spiritualwave.aphorisms.MainActivity
import org.spiritualwave.aphorisms.R

object NotificationUtils {

    private val TAG = NotificationUtils::class.simpleName
    private const val CHANNEL_ID = "org_spiritualwave_channelid"
    private const val CHANNEL_NAME = "org_spiritualwave_channelname"
    private const val CHANNEL_DESCRIPTION = "org_spiritualwave_description"

    @SuppressLint("LaunchActivityFromNotification")
    fun showNotification(context: Context, title: String, message: String) {
        val notificationId = 1 // Unique ID for the notification

        // Create an intent to launch the NotificationReceiver
        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent =
            PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE)

        // Build the notification using NotificationCompat.Builder
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.mipmap.icon)
            .setContentTitle(title)
            .setContentText("")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        // Show the notification using the NotificationManager
        val notificationManager = NotificationManagerCompat.from(context)
        Log.d(TAG, "showNotification")
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(TAG, "permission not granted")
            return
        }
        Log.d(TAG, "Notification generated successfully")
        // Check if the device is running on Android Oreo or above
        // Create the notification channel
        val channel = NotificationChannel(
            CHANNEL_ID,
            CHANNEL_NAME,
            NotificationManager.IMPORTANCE_HIGH
        ).apply {
            description = CHANNEL_DESCRIPTION
        }

        // Register the notification channel with the system
        notificationManager.createNotificationChannel(channel)
        notificationManager.notify(notificationId, builder.build())
    }

}