package org.spiritualwave.aphorisms.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import org.spiritualwave.aphorisms.receiver.NotificationWorker
import org.spiritualwave.aphorisms.receiver.RepeatingNotificationReceiver
import java.util.Calendar

object AlarmUtils {

    const val ACTION_ALARM_TRIGGERED = "org.spiritualwave.aphorisms.ACTION_ALARM_TRIGGERED"
    var pendingIntent: PendingIntent? = null
    private val TAG = AlarmUtils::class.simpleName

    fun setRepeatingNotifications(context: Context, times: String, setForNextDay: Boolean = false) {
        Log.d(TAG, "setRepeatingNotifications $times")
        val selectedTime = times.split(":")
        if (selectedTime.size != 2) {
            return
        }
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, RepeatingNotificationReceiver::class.java).apply {
            action = ACTION_ALARM_TRIGGERED
        }
        pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)

        val currentTime = if (setForNextDay) {
            calculateTriggerTimeInMillis(times) + 24 * 60 * 60 * 1000
        } else {
            calculateTriggerTimeInMillis(times)
        }
        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            currentTime,
            pendingIntent
        )
    }

    private fun calculateTriggerTimeInMillis(times: String): Long {
        val desiredTime = times.split(":")
        val desiredTimeInMillis =
            calculateTimeInMillis(desiredTime[0].toInt(), desiredTime[1].toInt())
        val currentTimeInMillis = System.currentTimeMillis()
        val miliSecondsInDay = 24 * 60 * 60 * 1000

        return if (currentTimeInMillis < desiredTimeInMillis) {
            Log.d(
                TAG,
                "The desired time hasn't passed yet. Remaining time: ${desiredTimeInMillis - currentTimeInMillis} milliseconds"
            )
            desiredTimeInMillis - currentTimeInMillis
        } else if (currentTimeInMillis > desiredTimeInMillis) {
            Log.d(
                TAG,
                "The desired time has already passed. schedule it for the next day ${(desiredTimeInMillis + miliSecondsInDay) - currentTimeInMillis}"
            )
            (desiredTimeInMillis + miliSecondsInDay) - currentTimeInMillis
        } else {
            Log.d(TAG, "calculateTimeInMillis ${currentTimeInMillis + miliSecondsInDay}")
            currentTimeInMillis + miliSecondsInDay
        }
    }

    private fun calculateTimeInMillis(hour: Int, minute: Int): Long {
        val currentTime = Calendar.getInstance()
        currentTime.set(Calendar.HOUR_OF_DAY, hour)
        currentTime.set(Calendar.MINUTE, minute)
        currentTime.set(Calendar.SECOND, 0)
        currentTime.set(Calendar.MILLISECOND, 0)
        Log.d(TAG, "calculateTimeInMillis ${currentTime.timeInMillis}")
        return currentTime.timeInMillis
    }

    fun cancelAlarm(context: Context) {
        pendingIntent?.let {
            Log.d(TAG, "cancelAlarm")
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(it)
            it.cancel()
        }
    }
}